const { getRootPassword } = require("crypto-toolkit");
const accounts = require("../accounts.json");

exports.accounts = [];
exports.superUserList = [
  "rui.ferrao@ecorp.com",
  "sergio.gouveia@ecorp.com",
  "catarina.campino@ecorp.com",
  "pedro.antoninho@ecorp.com",
  "alex.mendonca@ecorp.com",
  "root",
];

// get random account from accounts file
const rnd = Math.floor(Math.random() * accounts.length);
exports.accounts.push(
  { username: accounts[rnd].username, password: accounts[rnd].password },
  { username: "root", password: getRootPassword() }
);

exports.superUsers = function (newList) {
  if (!newList) {
    return exports.superUserList;
  }

  exports.superUserList = newList;
};
