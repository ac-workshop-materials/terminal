exports.get = function(file) {
    var content;

    switch (file) {
        case '/var/logs/ecorp.txt':
            content = '---START LOG---' + '\n' +
                '[12/03/2017 12:54] Philip Price;CEO;be859bd723182176c87b49cad69e6908;VISAPLATINUM4112436012348765;202301;228;' + '\n' +
                '[12/03/2017 18:01] Tyrell Wellick;SeniorVPofTechnology;cb5929d897358fe47389add2374b9b66;VISAGOLD4881555532159812;20160531;442;' + '\n' +
                '[12/03/2017 20:23] <AC/>;silentPartner;a0ef3158624b8ed0df5ec58efec8f433;PT50000700000019632790423;' + '\n' +
                '[14/03/2017 08:52] Scott Knowles;CTO;98a4192beb4733252fdb9c806062ea7f;VISAGOLD4448123400129746;20210910;222;' + '\n' +
                '[15/03/2017 08:53] fsociety;359cf3979f24dff26a628664547b539f;gnome_kali_linux;usernameQWERTY_goldfish;pass:r4M1m4L3k1s3ll10T;' + '\n' +
                '[15/03/2017 09:02] Susan Jacobs;General Counsel;0813478edcc022b79891625b78460c08;VISA4123765492834441;20171231;234;' + '\n' +
                '[15/03/2017 11:10] Angela Moss;Risk Management;personalPhoneNumber+1555123957121;' + '\n' +
                '---END LOG---';
            break;
        case '/var/easter/egg.txt':
            content = 'this is an easter egg.. [INSERT LORE HERE]';
            break;
    }

    return content;
};
