exports.CONNECTION_ESTABLISHED = 'Connection established to intranet.ecorp.com\n';
exports.ALREADY_LOGGED_IN = 'You are already logged in as %s. Logging out...';
exports.SUSPICIOUS = 'SUSPICIOUS ACTIVITY HAS BEEN DETECTED';
exports.MAX_ATTEMPTS = 'TERMINAL BLOCKED. SECURITY BREACH';
exports.NO_MORE_LOGINS = 'NO MORE LOGINS WILL BE ALLOWED FROM YOUR IP';
exports.ERR_SEP = '#######################################';
exports.LOGIN_TIMEOUT = 'Login aborted. Please wait %s seconds between each attempt.\n';
exports.LOGGING_IN = 'login --secure %s:%s';
exports.LOGGED_IN_MESSAGE = 'Welcome to the private ECorp network, %s\n';
exports.WRONG_CREDENTIALS = 'Invalid username/password combination. Please try again.\n';
exports.GET_FILE = 'get_file %s';
exports.GET_FILE_NOT_LOGGED = 'File access error: You are not logged in. Please login first to access the filesystem.';
exports.GET_FILE_NO_PERMISSIONS = 'File access error: You are not in the sudoers file.';
exports.DOWNLOADING_FILE = 'Downloading %s...';
exports.DOWNLOADED_FILE = 'Downloaded %s';
exports.PRINTING_FILE = 'Contents of %s:';
exports.FILE_NOT_FOUND = 'File %s not found!';
exports.LOGGED_IN_SPLASH = '                                                                                ' +
    '                                                                                \n' +
    '                                                                                \n' +
    '                                          ``                                    \n' +
    '                                       `/hMN/                                   \n' +
    '                                     :yNMMMMMy`                                 \n' +
    '                                  -smMMMMMMMMMm:                                \n' +
    '                               .+dMMMMMMMMMMMMMd.                               \n' +
    '                            `/hMMMMMMMMMMMMMmo.                                 \n' +
    '                          :yNMMMMMMMMMMMMNs-     -`                             \n' +
    '                       -sNMMMMMMMMMMMMNy:     .omMm:                            \n' +
    '                    .omMMMMMMMMMMMMMh/`    `+dMMMMMMs                           \n' +
    '                 `+dMMMMMMMMMMMMMd+`    `/hMMMMMMMMMMd-                         \n' +
    '               `hMMMMMMMMMMMMMMMh`    :yNMMMMMMMMMMMMNs                         \n' +
    '                :NMMMMMMMMMMMMMMMm:-sNMMMMMMMMMMMMNy:     .`                    \n' +
    '                 `yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh+`    `+dMm-                   \n' +
    '                   /NMMMMMMMMMMMMMMMMMMMMMMMMmo.     /hMMMMMMs                  \n' +
    '                    .hMMMMMMMMMMMMMMMMMMMMNs-     :yNMMMMMMMMMd-                \n' +
    '                      +MMMMMMMMMMMMMMMMMh:     .omMMMMMMMMMMMMMy`               \n' +
    '                       .dMMMMMMMMMMMMMMMN/  `+dMMMMMMMMMMMMMd+`                 \n' +
    '                         oMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMmo.                    \n' +
    '                          -mMMMMMMMMMMMMMMMMMMMMMMMMMMNs-                       \n' +
    '                           `yMMMMMMMMMMMMMMMMMMMMMMNy:                          \n' +
    '                             :NMMMMMMMMMMMMMMMMMMh/`                            \n' +
    '                              `hMMMMMMMMMMMMMMd+`                               \n' +
    '                                /NMMMMMMMMMmo.                                  \n' +
    '                                 .dMMMMMNy:                                     \n' +
    '                                   oMMh/                                        \n' +
    '                                    .`                                          \n' +
    '                                                                                \n' +
    '           .:::::::.        .///:`     `://:`    ::::::-`   ::::::.             \n' +
    '           sMdyyyyy/      :mNyosdNs  .hMhsshMd-  NMysssmMs  NMsssyMm.           \n' +
    '           sMs`````      .MN.    +o.`NM:    :MN` NM.   :Mm  NM.   dM+           \n' +
    '           sMNddddd`     /Md        -MN      NM- NMmmmmNs.  NMdhdmms`           \n' +
    '           sMo           .NM-   `yh.`mM+    +Mm` NM.  -mN-  NM-``               \n' +
    '           sMmhhhhhs      -hMdyhNm+  `yNmyymNy`  NM.   :Mm` NM.                 \n' +
    '           `........        `---.       .--.     ..     ..` ..                  \n' +
    '                                                                                ';
