var users = require('./users');
var sleep = require('sleep');

exports.enoughLoginInterval = function(lastLoginDate, minIntervalSeconds) {

    if (!lastLoginDate) {
        return true;
    }

    var lastLoginDelta = new Date().getTime() - lastLoginDate.getTime();

    return lastLoginDelta >= minIntervalSeconds * 1000;
};

exports.authenticate = function(username, password, secondsToWait) {

    sleep.msleep(secondsToWait * 1000);

    return users.accounts.some(function(u) {
        return u.username === username && u.password === password;
    });
};
