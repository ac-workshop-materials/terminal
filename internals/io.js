var chalk = require('chalk');
var loggedUser, file;

var styles = {
    prompt: chalk.bold,
    default: chalk.white,
    error: chalk.red.bold,
    success: chalk.green.bold
};

exports.styles = styles;
exports.DEFAULT_DOMAIN = 'ecorp.com';
exports.OUTPUT_SEP = ' >  ';
exports.PROMPT = '>_$ ';

exports.message = function(chalkStyle, msg, variables, prompt) {

    if (prompt) {
        msg = [exports.prompt(exports.PROMPT) + chalkStyle(msg)];
    } else {
        msg = [chalkStyle(exports.OUTPUT_SEP + msg)];
    }

    if (variables) {
        msg = msg.concat(variables);
    }

    console.log.apply(null, msg);
};

exports.stderr = function(msg, variables) {
    exports.message(styles.error, msg, variables, false);
};

exports.stdin = function(msg, variables) {
    exports.message(styles.default, msg, variables, true);
};

exports.stdout = function(msg, variables) {
    exports.message(styles.default, msg, variables, false);
};

exports.success = function(msg, variables) {
    exports.message(styles.success, msg, variables, false);
};

// TODO: this is not cool at all. find a better way to concatenate the prompt.
exports.prompt = function(msg) {
    if (!loggedUser) {
        return styles.prompt(exports.DEFAULT_DOMAIN) + msg;
    }

    return styles.prompt(loggedUser) + msg;
};

exports.setUser = function(username) {
    loggedUser = username;
};
