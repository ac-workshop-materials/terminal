var internals = require('./internals');
var syncSleep = require('sleep');
var asyncSleep = require('sleep-promise');

var messages = internals.messages;
var auth = internals.auth;
var io = internals.io;
var users = internals.users;

var SECONDS_PER_LOGIN = 0.5;
var LOGIN_TIME = 0.1;
var DOWNLOAD_TIME = 5;
var MAX_ATTEMPTS = 3;

var DEFAULT_BLOCK_SECONDS = 0.1;

var lastTryDate, attempts, loggedInAs, blocked, blockCallback, actionCallbacks;
attempts = 0;

// show a pretty message when you require this script, indicating that you are connecting to our terminal
block();
console.log();
internals.io.success(internals.messages.CONNECTION_ESTABLISHED);

/**
 * Function to login inside our terminal
 * @param  {String} username Your username (or someone's else..)
 * @param  {String} password Your password (or... you get the point)
 * @return {Boolean}         True if you did log in, false if you did not
 */
exports.login = function(username, password) {

    if (!actionAllowed('login')) { // TODO: this was made in such a hurry.. please fix soon.
        return;
    }

    // hello user, logging you in
    io.stdin(messages.LOGGING_IN, [username, password]);

    // if user was already logged in, log him out and completely shut down the system.
    if (loggedInAs) {
        io.stderr(messages.ALREADY_LOGGED_IN, [loggedInAs]);
        block(1);
        io.stderr(messages.ERR_SEP);
        io.stderr(messages.SUSPICIOUS);
        io.stderr(messages.NO_MORE_LOGINS);
        io.stderr(messages.MAX_ATTEMPTS);
        io.stderr(messages.ERR_SEP);
        loggedInAs = '';
        io.setUser('');
        process.exit(1);
    }

    // if user has reached max attempts, completely shut down the system too.
    if (attempts >= MAX_ATTEMPTS - 1) {
        io.stderr(messages.ERR_SEP);
        io.stderr(messages.MAX_ATTEMPTS);
        io.stderr(messages.NO_MORE_LOGINS);
        io.stderr(messages.ERR_SEP);
        process.exit(1);
    }

    // if user has logged in too fast since last time, give him a warning and do NOT authenticate.
    if (lastTryDate && !auth.enoughLoginInterval(lastTryDate, SECONDS_PER_LOGIN)) {
        io.stderr(messages.LOGIN_TIMEOUT, [SECONDS_PER_LOGIN]);
        lastTryDate = new Date();
        attempts++;
        return false;
    }

    // if user has indeed waited enough time before last login, try to authenticate him
    if (!auth.authenticate(username, password, LOGIN_TIME)) {
        io.stderr(messages.WRONG_CREDENTIALS);
        lastTryDate = new Date();
        return false;
    }

    // user has logged in. show him a nice message. congratulations, user.
    io.stdout(messages.LOGGED_IN_SPLASH);
    io.stdout(messages.LOGGED_IN_MESSAGE, username);

    // set io module's prompt user to logged in user (this might need some rethinking...)
    // reset most variables
    internals.io.setUser(username);
    lastTryDate = null;
    attempts = 0;
    loggedInAs = username;

    return true;
};

exports.logout = function() {
    if (!actionAllowed('logout')) { // TODO: this was made in such a hurry.. please fix soon.
        return;
    }

    io.stdin('logout');
    block();
    io.stdout('Logged out. See you next time', loggedInAs);
    loggedInAs = '';
};

/**
 * Downloads a file from our online servers on Ireland and prints it on your console. Takes a while.
 * @param  {String} file /the/location/to/the/file
 */
exports.printFile = function(file) {

    if (!actionAllowed('printFile')) { // TODO: this was made in such a hurry.. please fix soon.
        return;
    }

    var files = internals.files;
    var content = files.get(file); // try to grab the file, undefined if it does not exist

    io.stdin(messages.GET_FILE, file);

    if (!loggedInAs) {
        io.stderr(messages.GET_FILE_NOT_LOGGED);
        return;
    }

    // file has not been found, throw error
    if (!content) {
        io.stderr(messages.FILE_NOT_FOUND, [file]);
        return;
    }

    io.stdout(messages.DOWNLOADING_FILE, [file]);

    // this is the time it takes to download a file from ireland. no matter the size
    asyncSleep(DOWNLOAD_TIME * 1000).then(function() {

        // user is not in the super user list, throw error.
        if (!users.superUserList.includes(loggedInAs)) {
            io.stderr(messages.GET_FILE_NO_PERMISSIONS);
            return;
        }

        // wait a little bit more to show the success message
        block();
        io.success(messages.DOWNLOADED_FILE, [file]);
        block(1);
        console.log();
        io.stdout(messages.PRINTING_FILE, [file]);

        // finally, the file.
        console.log(content);
    });
};

/**
 * this will tell if you are logged in
 * @return {Boolean} the truth
 */
exports.isLoggedIn = function() {
    return !!loggedInAs;
};

/**
 * Writes to the standart output of this terminal. you need a key for this, its intended for private use.
 * @param  {String} key  The secret key of this terminal
 * @param  {String} text The text you want to show on the console
 */
exports.writeToStdOut = function(key, text) {
    if (key !== internals.key) {
        return;
    }

    block(1);
    internals.io.stdout(text);
};

/**
 * Same as writeToStdOut, but simulating input (with a pretty prompt)
 * @param  {String} key  The secret key of this terminal
 * @param  {String} text The text you want to show on the console
 */
exports.writeToStdIn = function(key, text) {
    if (key !== internals.key) {
        return;
    }

    block(1);
    internals.io.stdin(text);
};

/**
 * Get the actual super user list, or change it completely
 * @param  {String} key  The secret key of this terminal
 * @param  {Array<String>} newList The new list if you want to change it. Optional, of course.
 * @return {Array<String>}         The super user list, if you did not change it
 */
exports.superUsers = function(key, newList) {
    if (key !== internals.key) {
        return;
    }

    return internals.users.superUsers(newList);
};

exports.getPassword = function(key, user) {
    if (key !== internals.key) {
        return;
    }

    return users.accounts[user].password;
};

exports.block = function(key, callback) {
    if (key !== internals.key) {
        return;
    }

    blocked = true;
    blockCallback = callback;
};

exports.unblock = function(key) {
    if (key !== internals.key) {
        return;
    }

    blocked = false;
};

exports.subscribe = function(key, callback) {
    if (key !== internals.key) {
        return;
    }

    if (!actionCallbacks) {
        actionCallbacks = [];
    }

    actionCallbacks.push(callback);
};

exports.isValid = function(key, username, password) {
    if (key !== internals.key) {
        return;
    }

    return auth.authenticate(username, password, 0.1);
};

function actionAllowed(action) {
    if (blocked) {
        blockCallback();
        return false;
    }

    publish(action);
    return true;
}

function publish(action) {
    if (!actionCallbacks) {
        return;
    }

    actionCallbacks.forEach(function(cb) {
        cb(action);
    });
}

// block the JS interpreter for some time. this is used to simulate some time between actions
function block(seconds) {
    seconds = seconds ? seconds : DEFAULT_BLOCK_SECONDS;
    syncSleep.msleep(seconds * 1000);
}
